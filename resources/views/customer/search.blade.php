@extends('layouts.customer') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Siuntų sąrašas</div>

				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
					<form class="form-horizontal" method="POST" action="{{ action('ParcelController@findParcel') }}">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="id" class="col-md-4 control-label">Siuntos kodas</label>

							<div class="col-md-6">
								<input id="id" type="text" class="form-control" name="id" value="{{ old('id') }}" required autofocus>
							</div>
						</div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Ieškoti
                                </button>
                            </div>
                        </div>
					</form>
					@if(isset($parcel))
                    <h2 class="control-label">Siunta {{$id}}</h2>
					<table class="table table-striped">
						<tr>
							<th>Būsena</th>
							<th>Data</th>
						</tr>
                        @foreach($parcel as $state)
						<tr>
							@switch($state->state) @case(0)
							<td>Priimta</td>
							@break @case(1)
							<td>Išsiųsta</td>
							@break @case(2)
							<td>Gauta</td>
							@break @case(3)
							<td>Įteikta</td>
							@break @endswitch
							<td>{{$state->date}}</td>
						</tr>
                        @endforeach
					</table>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection