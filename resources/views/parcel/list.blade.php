@extends('layouts.operator') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Siuntų sąrašas</div>

				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
					<table class="table table-striped">
						<tr>
							<th>Siunta</th>
							<th>Būsena</th>
							<th>Data</th>
							<th>Redaguoti</th>
						</tr>@foreach($states as $state)
						<tr>
							<td>{{$state->parcel_id}}</td>
							@switch($state->state)
								@case(0)
								<td>Priimta</td>
								@break
								@case(1)
								<td>Išsiųsta</td>
								@break
								@case(2)
								<td>Gauta</td>
								@break
								@case(3)
								<td>Įteikta</td>
								@break
							@endswitch
							<td>{{$state->date}}</td>
							<td>
								<a href="{!!route('edit-parcel', ['id' => $state->parcel_id])!!}" class="btn btn-xs btn-info">Redaguoti</a>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection