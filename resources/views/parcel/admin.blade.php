@extends('layouts.admin') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Įvykdytų siuntų sąrašas</div>

				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
          
          @if(count($parcels) < 1)
          <h2>Įvykdytų siuntų nėra!</h2>
          @else
					<table class="table table-striped">
						<tr>
							<th>Siunta</th>
							<th>Trukmė</th>
						</tr>@foreach($parcels as $parcel)
						<tr>
							<td>{{$parcel['parcel']}}</td>
							<td>{{$parcel['time']}}</td>
						</tr>
						@endforeach
					</table>
          @endif
				</div>
        
			</div>
      <div class="panel panel-default">
				<div class="panel-heading">Neįvykdytų siuntų sąrašas</div>

				<div class="panel-body">
					@if (session('status'))
					<div class="alert alert-success">
						{{ session('status') }}
					</div>
					@endif
          
          @if(count($parcels) < 1)
          <h2>Neįvykdytų siuntų nėra!</h2>
          @else
					<table class="table table-striped">
						<tr>
							<th>Siunta</th>
							<th>Būsena</th>
						</tr>@foreach($undone as $parcel)
						<tr>
							<td>{{$parcel->id}}</td>
								@switch($parcel->state)
								@case(0)
								<td>Priimta</td>
								@break
								@case(1)
								<td>Išsiųsta</td>
								@break
								@case(2)
								<td>Gauta</td>
								@break
								@case(3)
								<td>Įteikta</td>
								@break
							@endswitch
						</tr>
						@endforeach
					</table>
          @endif
				</div>
        
			</div>
		</div>
	</div>
</div>
@endsection