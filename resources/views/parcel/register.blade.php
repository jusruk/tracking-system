@extends('layouts.operator') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Registruoti naują siuntą</div>

				<div class="panel-body">
					<form class="form-horizontal" method="POST" action="{{ action('UserController@registerParcel') }}">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="id" class="col-md-4 control-label">Siuntos kodas</label>

							<div class="col-md-6">
								<input id="id" type="text" class="form-control" name="id" value="{{ old('id') }}" required autofocus>
							</div>
						</div>

						<div class="form-group">
							<label for="weight" class="col-md-4 control-label">Siuntos svoris (kilogramais)</label>

							<div class="col-md-6">
								<input id="weight" type="text" class="form-control" name="weight" value="{{ old('weight') }}" required>
							</div>
						</div>

						<div class="form-group">
							<label for="length" class="col-md-4 control-label">Siuntos ilgis (milimetrais)</label>

							<div class="col-md-6">
								<input id="length" type="text" class="form-control" name="length" value="{{ old('length') }}" required>
							</div>
						</div>

						<div class="form-group">
							<label for="width" class="col-md-4 control-label">Siuntos plotis (milimetrais)</label>

							<div class="col-md-6">
								<input id="width" type="text" class="form-control" name="width" value="{{ old('width') }}" required>
							</div>
						</div>

                        <div class="form-group">
							<label for="height" class="col-md-4 control-label">Siuntos aukštis (milimetrais)</label>

							<div class="col-md-6">
								<input id="height" type="text" class="form-control" name="height" value="{{ old('height') }}" required>
							</div>
						</div>

                        <div class="form-group">
							<label for="type" class="col-md-4 control-label">Siuntos tipas</label>

							<div class="col-md-6">
								<input id="type" type="text" class="form-control" name="type" value="{{ old('type') }}" required>
							</div>
						</div>

						<div class="form-group">
							<label for="office" class="col-md-4 control-label">Pasirinkite gavėjo siuntų tašką</label>

							<div class="col-md-6">
								<select name="office">
									@foreach($offices as $address => $id)
									<option value="{{$id}}">{{$address}}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Registruoti
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection