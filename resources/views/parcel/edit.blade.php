@extends('layouts.operator') @section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Keisti siuntos {{$parcel->id}} būseną</div>
                
				<form class="form-horizontal" method="POST" action="{{ action('ParcelController@updateParcel', ['id' => $parcel->id]) }}">
						{{ csrf_field() }}

						<div class="form-group">
							<label for="id" class="col-md-4 control-label">Siuntos būsena</label>

							<div class="col-md-6">
                                <select name="state">
                                    <option value="1">Išsiųsta</option>
                                    <option value="2">Gauta</option>
                                    <option value="3">Įteikta</option>
                                </select>
                            </div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Keisti būseną
								</button>
							</div>
						</div>
					</form>
			</div>
		</div>
	</div>
</div>
@endsection