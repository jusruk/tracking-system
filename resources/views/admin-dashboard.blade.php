@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Operatoriai</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-striped">
                    <tr>
                            <th>Vardas</th>
                            <th>Elektronins paštas</th>
                            <th>Siuntų taškas</th>
                        </tr>
                        @foreach($info as $operator)
                            <tr>
                            <td>{{$operator['user']->name}}</td>
                            <td>{{$operator['user']->email}}</td>
                            <td>{{$operator['office']->address}}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
