<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parcel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'weight', 'length', 'width', 
        'height', 'type', 'state', 
        'destination', 'operator'
    ];

    protected $casts = ['id' => 'string'];
}
