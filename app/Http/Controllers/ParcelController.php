<?php

namespace App\Http\Controllers;

use App\Parcel;
use Illuminate\Http\Request;
use App\State;
use Auth;
class ParcelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editParcel($id, Request $request){
        if ($request->user()->hasRole('Operatorius') === NULL)
            return redirect('/');
        $parcel = Parcel::where('id', '=', $id)->first();
        return view('parcel.edit', compact('parcel'));
    }

    public function updateParcel($id, Request $request){
        if ($request->user()->hasRole('Operatorius') === NULL)
            return redirect('/');
        $parcel = Parcel::where('id', '=', $id)->first();
        $states = State::where('parcel_id', '=', $id)->get();
        if(!is_array($states))
            $states = State::where('parcel_id', '=', $id)->orderBy('state', 'DESC')->first();
        if (is_array($states)){
            if (in_array($_POST['state'], $states)){
                return redirect()->action('ParcelController@editParcel')->with('failure', 'Būsena jau buvo įvesta');
            }
            else {
                $state = State::create([
                    'state' => $_POST['state'],
                    'date' => date("Y-m-d"),
                    'parcel_id' => $id
                ]);
                $state->save();
                return redirect()->action('UserController@showParcels', ['id' => $id])->with('success', "Siuntos būsena atnaujinta");
            }
        } else{
            if ($states->state >= $_POST['state']){
                return redirect()->back()->with('error', 'Būsena jau buvo įvesta');
            }
            else {
                $state = State::create([
                    'state' => $_POST['state'],
                    'date' => date("Y-m-d"),
                    'parcel_id' => $id
                ]);
                $state->save();
                return redirect()->action('UserController@showParcels', ['id' => Auth::user()->id])->with('success', "Siuntos būsena atnaujinta");
            }
        }
    }

    public function search(Request $request){
        if ($request->user()->hasRole('Klientas') === NULL)
            return redirect('/');
        return view('customer.search');
    }

    public function findParcel(Request $request){
        if ($request->user()->hasRole('Klientas') === NULL)
            return redirect('/');
        $id=$_POST['id'];
        $parcel = State::where('parcel_id', '=', $id)->orderBy('state', 'desc')->get();
        return view('customer.search', compact('parcel', 'id'));
    }
}
