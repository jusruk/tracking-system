<?php

namespace App\Http\Controllers;

use App\Office;
use App\Operator;
use App\Parcel;
use App\Role;
use App\State;
use App\Type;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function register(Request $request)
    {
        if ($request->user()->hasRole('Administratorius') === NULL)
            return redirect('/');
        $offices = Office::pluck('id', 'address');
        return view('operator.register', compact('offices', $offices));
    }

    public function customerRegistration(Request $request)
    {
        if ($request->user()->hasRole('Operatorius') === NULL)
            return redirect('/');
        return view('customer.register');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'office' => 'required|string',
        ]);
    }

    public function registerCustomer()
    {
        $user = User::create([
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => bcrypt($_POST['password']),
        ]);

        $user->roles()->attach(Role::where('name', 'Klientas')->first());
        $user->save();
        return redirect()->action('UserController@customerRegistration')->with('success', 'Klientas užregistruotas');
    }

    public function registerOperator()
    {

        $user = User::create([
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => bcrypt($_POST['password']),
        ]);

        $user->roles()->attach(Role::where('name', 'Operatorius')->first());

        $user->save();
        $user = User::where('email', $_POST['email'])->first();
        $operator = Operator::create([
            'user_id' => $user->id,
            'office_id' => $_POST['office'],
        ]);
        $operator->save();
        return redirect()->action('UserController@register')->with('success', 'Operatorius užregistruotas');
    }

    public function showOperators()
    {
        $info = array();
        $operators = Operator::get();
        foreach ($operators as $operator) {
            $user = User::where('id', $operator->user_id)->first();
            $office = Office::where('id', $operator->office_id)->first();
            $info[] = array('user' => $user, 'office' => $office);
        }
        return view('admin-dashboard', compact('info', $info));
    }

    public function showParcels($id, Request $request)
    {
        if ($request->user()->hasRole('Operatorius') === NULL)
            return redirect('/');
        $operator = Operator::where('user_id', '=', $id)->first();
        $parcels = Parcel::where('destination', '=', $operator->office_id)->where('state', '!=', 3)->get();
        $states = array();
        foreach ($parcels as $i => $parcel) {
            $state = State::where('parcel_id', '=', $parcel->id)->orderBy('state', 'desc')->first();

            $states[$i] = $state;
        }
        return view('parcel.list', compact('parcels', 'states'));
    }

    public function parcelRegistration(Request $request)
    {
        if ($request->user()->hasRole('Operatorius') === NULL)
            return redirect('/');
        $offices = Office::pluck('id', 'address');
        return view('parcel.register', compact('offices', $offices));
    }

    public function registerParcel(Request $request)
    {
        $type = $_POST['type'];
        if (!Type::where('info', '=', $type)->exists()) {
            $type = Type::create([
                'info' => $_POST['type'],
            ]);
            $type->save();
        }
        $state = State::create([
            'state' => '0',
            'parcel_id' => $_POST['id'],
            'date' => date("Y-m-d"),
        ]);
        $state->save();
        $state = 'priimta';
        if (!is_numeric($_POST['weight'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos svoris turi būti skaičius!');
        }
        if ($_POST['weight'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos svoris turi būti teigiamas skaičius!');
        }
        if (!is_numeric($_POST['length'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos ilgis turi būti skaičius!');
        }
        if ($_POST['length'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos ilgis turi būti teigiamas skaičius!');
        }
        if (!is_numeric($_POST['width'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos plotis turi būti skaičius!');
        }
        if ($_POST['width'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos plotis turi būti teigiamas skaičius!');
        }
        if (!is_numeric($_POST['height'])) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos aukštis turi būti skaičius!');
        }
        if ($_POST['height'] < 0) {
            $request->flash();
            return redirect()->back()->withInput()->with('error', 'Siuntos aukštis turi būti teigiamas skaičius!');
        }
        $parcel = Parcel::create([
            'id' => $_POST['id'],
            'weight' => $_POST['weight'],
            'length' => $_POST['length'],
            'width' => $_POST['width'],
            'height' => $_POST['height'],
            'type' => Type::where('info', '=', $_POST['type'])->first()->id,
            'state' => State::where('state', '=', 0)->where('parcel_id', '=', $_POST['id'])->first()->id,
            'destination' => $_POST['office'],
            'operator' => $request->user()->id,
        ]);
        $parcel->save();
        return redirect()->action('UserController@parcelRegistration')->with('success', 'Siunta užregistruota');
    }

    public function parcels(Request $request)
    {
        if ($request->user()->hasRole('Administratorius') === NULL)
            return redirect('/');
        $statesStart = State::where('state', '=', 0)->orderBy('parcel_id')->get();
        $statesDone = State::where('state', '=', 3)->orderBy('parcel_id')->get();
        $parcels = array();
        foreach ($statesDone as $done) {
            foreach ($statesStart as $start) {
                if ($done->parcel_id === $start->parcel_id) {
                    $parcel = $done->parcel_id;
                    $time = $this->dateDiff($done->date, $start->date);
                    if (strpos($time, 'year') !== false) {
                        $time = str_replace('years', 'metus', $time);
                        $time = str_replace('year', 'metus', $time);
                    }
                    if (strpos($time, 'month') !== false) {
                        $time = str_replace('months', 'mėnesių', $time);
                        $time = str_replace('month', 'mėnesį', $time);
                    }
                    if (strpos($time, 'day') !== false) {
                        $time = str_replace('days', 'dienų', $time);
                        $time = str_replace('day', 'dieną', $time);
                    }
                    $parcels[] = array('time' => $time, 'parcel' => $parcel);
                }
            }
        }
        $undone = Parcel::where('state', '!=', 3)->get();

        return view('parcel.admin', compact('parcels', 'undone'));
    }

    public function dateDiff($time1, $time2, $precision = 6)
    {
        // If not numeric then convert texts to unix timestamps
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }

        // If time1 is bigger than time2
        // Then swap time1 and time2
        if ($time1 > $time2) {
            $ttime = $time1;
            $time1 = $time2;
            $time2 = $ttime;
        }

        // Set up intervals and diffs arrays
        $intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');
        $diffs = array();

        // Loop thru all intervals
        foreach ($intervals as $interval) {
            // Create temp time from time1 and interval
            $ttime = strtotime('+1 ' . $interval, $time1);
            // Set initial values
            $add = 1;
            $looped = 0;
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
                // Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
            }

            $time1 = strtotime("+" . $looped . " " . $interval, $time1);
            $diffs[$interval] = $looped;
        }

        $count = 0;
        $times = array();
        // Loop thru all diffs
        foreach ($diffs as $interval => $value) {
            // Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
            // Add value and interval
            // if value is bigger than 0
            if ($value > 0) {
                // Add s if value is not 1
                if ($value != 1) {
                    $interval .= "s";
                }
                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }

        // Return string with times
        return implode(", ", $times);
    }
}
