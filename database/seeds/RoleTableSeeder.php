<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'Administratorius';
        $role_admin->description = 'Sistemos administratorius';
        $role_admin->save();

        $role_operator = new Role();
        $role_operator->name = 'Operatorius';
        $role_operator->description = 'Siuntų taško operatorius';
        $role_operator->save();

        $role_customer = new Role();
        $role_customer->name = 'Klientas';
        $role_customer->description = 'Sistemos klientas';
        $role_customer->save();
    }
}
