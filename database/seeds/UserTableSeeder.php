<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Administratorius')->first();
        $role_operator = Role::where('name', 'Operatorius')->first();
        $role_customer = Role::where('name', 'Klientas')->first();

        $admin = new User();
        $admin->name = 'Administratorius test';
        $admin->email = 'administratorius@test.com';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $operator = new User();
        $operator->name = 'Operatorius test';
        $operator->email = 'operatorius@test.com';
        $operator->password = bcrypt('operator');
        $operator->save();
        $operator->roles()->attach($role_operator);

        $customer = new User();
        $customer->name = 'Klientas test';
        $customer->email = 'klientas@test.com';
        $customer->password = bcrypt('customer');
        $customer->save();
        $customer->roles()->attach($role_customer);
    }
}
