<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

Route::resource('user', 'UserController');

Route::get('/parcel/register', 'UserController@parcelRegistration');
Route::post('/parcel/register', 'UserController@registerParcel');
Route::get('/parcels/search', 'ParcelController@search');
Route::post('parcels/search', 'ParcelController@findParcel');
Route::get('/parcels/{id}',[
    'uses' => 'UserController@showParcels',
    'as' => 'parcels'
    ]);
Route::get('/parcels/edit/{id}', [
    'uses' => 'ParcelController@editParcel',
    'as' => 'edit-parcel'
]);
Route::post('/parcels/edit/{id}','ParcelController@updateParcel');
Route::get('/operator/register', 'UserController@register');
Route::post('operator/register', 'UserController@registerOperator');
Route::get('/admin/dashboard', 'UserController@showOperators');
Route::get('/admin/parcels', 'UserController@parcels');
Route::get('/customer/register', 'UserController@customerRegistration');
Route::post('/customer/register', 'UserController@registerCustomer');